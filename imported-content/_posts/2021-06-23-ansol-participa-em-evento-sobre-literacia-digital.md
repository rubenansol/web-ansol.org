---
categories:
- evento
- volt
- livre
- ansol
- nomadismo digital
metadata:
  tags:
  - tags_tid: 84
  - tags_tid: 337
  - tags_tid: 338
  - tags_tid: 33
  - tags_tid: 339
  node_id: 805
layout: article
title: ANSOL participa em evento sobre Literacia Digital
created: 1624485332
date: 2021-06-23
---
<p>Num convite feito pelo VOLT Portugal, a ANSOL esteve presente num evento sobre Literacia Digital, onde Tiago Carrondo, presidente da ANSOL, falou sobre a importância do Software Livre. O evento, co-organizado com o Livre, teve também a particição de <span class="d2edcug0 hpfvmrgz qv66sw1b c1et5uql b0tq1wua a8c37x1j keod5gw0 nxhoafnm aigsh9s9 d9wwppkn fe6kdd0r mau55g9w c8b282yb hrzyx87i jq4qci2q a3bd9o3v knj5qynh oo9gr5id" dir="auto">Krystel Leal, freelancer em Silicon Valley na área de desenvolvimento web e fundadora do projecto Nomadismo Digital Portugal, e Paulo Maia, Mestre em Bioengenharia a exercer agora como data scientist.</span></p><p><span class="d2edcug0 hpfvmrgz qv66sw1b c1et5uql b0tq1wua a8c37x1j keod5gw0 nxhoafnm aigsh9s9 d9wwppkn fe6kdd0r mau55g9w c8b282yb hrzyx87i jq4qci2q a3bd9o3v knj5qynh oo9gr5id" dir="auto">A gravação do evento pode ser vista aqui:</span></p><p><iframe style="display: block; margin-left: auto; margin-right: auto;" src="https://archive.org/embed/hoje-vamos-falar-de-literacia-digital-explicando-a-importancia-de-softwares-...-322351522628976" width="640" height="480" frameborder="0"></iframe></p>
