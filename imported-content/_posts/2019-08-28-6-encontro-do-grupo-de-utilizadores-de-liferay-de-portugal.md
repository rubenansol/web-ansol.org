---
categories: []
metadata:
  event_site:
  - event_site_url: https://www.meetup.com/Liferay-Portugal-User-Group/events/261702314/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-09-17 23:00:00.000000000 +01:00
    event_start_value2: 2019-09-17 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 691
layout: evento
title: 6º Encontro do Grupo de Utilizadores de Liferay de Portugal
created: 1567011287
date: 2019-08-28
---

