---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://www.classy.org/event/a-new-era-of-open-covid-19-and-the-pursuit-for-equitable-solutions/e327790
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-03-16 00:00:00.000000000 +00:00
    event_start_value2: 2021-03-16 00:00:00.000000000 +00:00
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 781
layout: evento
title: A New Era of Open? COVID-19 and the Pursuit for Equitable Solutions
created: 1615655745
date: 2021-03-13
---
<p><span style="font-size: 17px;"><span style="font-family: source sans pro,helvetica neue,helvetica,arial,sans-serif;"> In this panel, we’ll examine the fields of Open Data, Open Science, and Open Source Medical Hardware with leading experts and practitioners, asking questions like: “What does “open” mean in the COVID-19 context?” “What role can open access and the open community play in ensuring there is timely and equitable access to medical and scientific research outputs and data, vaccines and treatments?” “Can open science and open data help prevent the next pandemic?” <br></span></span></p>
