---
categories: []
metadata:
  event_location:
  - event_location_value: Heidelberg, Germany
  event_site:
  - event_site_url: https://2019.stateofthemap.org/
    event_site_title: State of the Map
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-09-20 23:00:00.000000000 +01:00
    event_start_value2: 2019-09-22 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: !binary |-
      AQEAAAAAAAAAWFohQPay+9BRtEhA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.49408746836567e2
    mapa_lon: !ruby/object:BigDecimal 27:0.8676452636719e1
    mapa_left: !ruby/object:BigDecimal 27:0.8676452636719e1
    mapa_top: !ruby/object:BigDecimal 27:0.49408746836567e2
    mapa_right: !ruby/object:BigDecimal 27:0.8676452636719e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.49408746836567e2
    mapa_geohash: u0y1j6tfzczzrbzg
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 70
  node_id: 657
layout: evento
title: State of the Map 2019
created: 1552070544
date: 2019-03-08
---
<p>State of the Map is the annual event for <em>all mappers and OpenStreetMap users</em>. Enjoy three days in the heart of Europe with talks, discussions, workshops, code and documentation sprints all around the free and open map of the world.</p><p>Join us for a three day event around OpenStreetMap in <a href="https://www.openstreetmap.org/node/240058050#map=14/49.4102/8.6970">Heidelberg</a> for talks, discussions, workshops, code and documentation sprints.</p><h2 class="space-bottom2 space-top4">Our motto: Bridging the Map</h2><p>Right next to the old bridge in Heidelberg there lives a curious little statue – a bronze monkey. In its hand, it holds a mirror to remind anyone crossing the bridge to look back from where they came from and remember who they are. For the <span class="numeral">2019</span> SotM in Heidelberg we want to take the role of that little monkey and remind everyone that no matter where we are going and what we are doing with OSM, we should never forget that we all come from the same origin: a little map that anyone can modify and use. Whether you are a hobby mapper, a scientific researcher, a humanitarian, an NGO, a government agency, a small business or a global company — we want to bridge the gap between us, or better yet, we want to Bridge the Map.</p>
