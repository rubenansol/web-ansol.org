---
categories:
- 20 anos
- aniversário
- ansol
- software livre
- evento
- noticia
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 340
  - tags_tid: 341
  - tags_tid: 33
  - tags_tid: 41
  - tags_tid: 84
  node_id: 816
  event:
    location: Online
    site:
      title: ''
      url: https://ansol.org/20anos
    date:
      start: 2021-10-09 15:00:00.000000000 +01:00
      finish: 2021-10-09 18:00:00.000000000 +01:00
layout: evento
title: ANSOL - 20 anos de Software Livre
date: 2021-09-12
aliases:
- "/20anos/"
- "/evento/816/"
- "/node/816/"
---

Para celebrar o seu 20º aniversário, a ANSOL organizou dia 9 de Outubro das 15h
às 18h um ciclo de apresentações onde foram analisados os últimos 20 anos do
Software Livre em Portugal e discutidos os próximos 20 anos.

Contámos com a presença do actual e dos antigos presidentes da associação e com
a presença de Matthias Kirschner, actual presidente da [Free Software
Foundation Europe](https://fsfe.org/). Estiveram também presentes
representantes de várias associações nacionais como a [D3 - Defesa dos Direitos
Digitais](https://direitosdigitais.pt), [ANPRI - Associação Nacional de
Professores de Informática](https://www.anpri.pt/) e a [Wikimedia
Portugal](https://pt.wikimedia.org/wiki/Wikimedia_Portugal).

## Programa:

- 15h00 - 15h30: Abertura (Tiago Carrondo)
- 15h30 - 16h30: Mesa redonda com antigos presidentes (moderação Rute Correia)
- 16h30 - 17h00: Free Software Foundation Europe (Matthias Kirschner)
- 17h00 - 17h20: Wikimedia PT
- 17h20 - 17h40: ANPRI - Associação Nacional de Professores de Informática
- 17h40 - 18h00: D3 - Defesa dos Direitos Digitais
- 18h00: Encerramento

O evento foi online, de entrada gratuita e sem necessidade de registo prévio.

Uma gravação do evento pode ser agora vista aqui:

<iframe src="https://archive.org/embed/ansol-20anos&amp;playlist=1" width="640" height="480" frameborder="0"></iframe>

**Vídeos extra:**

Além do evento em directo, há aqui alguns vídeos para ver com calma:

<iframe src="https://archive.org/embed/CryptomoedasForkthis"
        width="640" height="480"
        frameborder="0"></iframe>
<iframe src="https://archive.org/embed/bastidores-da-marmita-como-manter-um-podcast-a-custo-zero"
        width="640" height="480"
        frameborder="0"></iframe>
<iframe src="https://archive.org/embed/internet_dos_pobrezinhos"
        width="640" height="480"
        frameborder="0"></iframe>
<iframe src="https://player.vimeo.com/video/279811204?h=4ba583c16d"
        width="640" height="360"
        frameborder="0"></iframe>
<iframe src="https://archive.org/embed/fsfe15pt"
        width="640" height="480"
        frameborder="0"></iframe>
