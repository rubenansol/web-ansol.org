---
categories:
- privacidade
- direitos digitais
- liberdade
- cultura livre
metadata:
  event_location:
  - event_location_value: Dresden, Alemanha
  event_site:
  - event_site_url: https://www.datenspuren.de/2015/
    event_site_title: Datenspuren 2015
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-10-23 23:00:00.000000000 +01:00
    event_start_value2: 2015-10-24 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 112
  - tags_tid: 113
  - tags_tid: 114
  - tags_tid: 111
  node_id: 357
layout: evento
title: Datenspuren 2015
created: 1445726655
date: 2015-10-24
---
<p>O Datenspuren 2015&nbsp;quer lidar com o significado e definição da privacidade (digital) na era da Internet das Coisas e de Snowden. Porque a nossa privacidade (depois de Snowden) é restringida por um lado, por nós mesmos através de novos telefones inteligentes e por outro lado pelas autoridades públicas.</p><p>Aqui uma nova consciência é necessária e os limites devem ser redefinidos para empresas e governo.</p><p>Info-Wiki:<a href="https://wiki.c3d2.de/Datenspuren_2015" style="font-size: 13.008px; line-height: 1.538em;">https://wiki.c3d2.de/Datenspuren_2015</a></p><dl><dt>Etherpad:<a href="https://pentapad.c3d2.de/p/Datenspuren_2015_allgemein" style="font-size: 13.008px; line-height: 1.538em;">https://pentapad.c3d2.de/p/Datenspuren_2015_allgemein</a></dt><dt></dt><dt>Orga-Wiki:<a href="https://wiki.c3d2.de/Datenspuren_2015/Organisation" style="font-size: 13.008px; line-height: 1.538em;">https://wiki.c3d2.de/Datenspuren_2015/Organisation</a></dt><dt></dt><dt>Chat:IRC-Channel&nbsp;<a href="irc://irc.freenode.net/datenspuren" style="font-size: 13.008px; line-height: 1.538em;">#datenspuren</a>&nbsp;im&nbsp;<a href="http://freenode.net/" style="font-size: 13.008px; line-height: 1.538em;">Freenode</a>&nbsp;(<a href="https://webchat.freenode.net/?channels=datenspuren" style="font-size: 13.008px; line-height: 1.538em;">Webchat</a>)</dt><dt></dt><dt>E-Mail:<a href="mailto:datenspuren@c3d2.de" style="font-size: 13.008px; line-height: 1.538em;">datenspuren@c3d2.de</a>&nbsp;und&nbsp;<a href="https://lists.c3d2.de/cgi-bin/mailman/listinfo/datenspuren" style="font-size: 13.008px; line-height: 1.538em;">Mailing-Liste datenspuren@lists.c3d2.de</a></dt><dt></dt><dt>App:<a href="https://play.google.com/store/apps/details?id=info.metadude.android.datenspuren.schedule" style="font-size: 13.008px; line-height: 1.538em;">Fahrplan-App für Android</a>&nbsp;von Tobias Preuss (<a href="https://github.com/johnjohndoe/CampFahrplan/tree/datenspuren-2015" style="font-size: 13.008px; line-height: 1.538em;">Source</a>)</dt></dl>
