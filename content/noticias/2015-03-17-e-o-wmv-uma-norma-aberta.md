---
categories:
- normas abertas
- norma aberta
- wmv
- standard
metadata:
  tags:
  - tags_tid: 87
  - tags_tid: 88
  - tags_tid: 89
  - tags_tid: 90
  node_id: 295
layout: article
title: É o WMV uma norma aberta?
created: 1426631448
date: 2015-03-17
aliases:
- "/article/295/"
- "/node/295/"
---
<p>No sentido a responder publicamente a uma questão que nos foi feita chegar mais do que uma vez, a ANSOL aqui esclarece:</p><p>De acordo com a Lei 36/2011, uma norma é aberta se cumprir cumulativamente os seguintes requisitos:<br><br>a) A sua adopção decorra de um processo de decisão aberto e disponível à participação de todas as partes interessadas;<br>b) O respectivo documento de especificações tenha sido publicado e livremente disponibilizado, sendo permitida a sua cópia, distribuição e utilização, sem restrições;<br>c) O respectivo documento de especificações não incida sobre acções ou processos não documentados;<br>d) Os direitos de propriedade intelectual que lhe sejam aplicáveis, incluindo patentes, tenham sido disponibilizados de forma integral, irrevogável e irreversível ao Estado Português;<br>e) Não existam restrições à sua implementação.<br><br>Tendo os dois primeiros pontos em consideração, observa-se que:<br>a) O WMV foi aprovado como norma pela SMTPE, pelo que à partida, o ponto (a) estará cumprido;<br>b) O documento de especificações não é livremente disponibilizado. Segundo o <a href="https://www.smpte.org/news-events/news-releases/smpte-releases-vc-1-standard">sítio web da SMTPE</a>,</p><p><cite>The VC-1 documents are SMPTE 421M-2006, "VC-1 Compressed Video Bitstream Format and Decoding Process" - the Standard itself, as well as two supporting Recommended Practices, SMPTE RP227-2006 "VC-1 Bitstream Transport Encodings" and SMPTE RP228-2006 "VC-1 Decoder and Bitstream Conformance". All three documents can be purchased on the SMPTE website at www.smpte.org.".</cite></p><p>Assim, o ponto b) não é cumprido, pelo que <strong>a norma WMV não é considerável aberta, segundo a Legislação Nacional</strong>.</p>
