---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 343
  event:
    location: ISCTE-IUL, Lisboa
    site:
      title: ''
      url: http://www.esop.pt/Default/pt/Destaques/Noticia?A=49
    date:
      start: 2015-09-16 17:00:00.000000000 +01:00
      finish: 2015-09-16 17:00:00.000000000 +01:00
    map: {}
layout: evento
title: Debate público "POS2020 - Portugal Open Source 2020"
created: 1441724972
date: 2015-09-08
aliases:
- "/evento/343/"
- "/node/343/"
---
<p style="margin-bottom: 0.25cm; font-weight: normal; line-height: 100%;" align="justify"><span style="font-family: sans-serif;"><span style="font-size: medium;">Com o objetivo de colocar as TIC – Tecnologias de Informação e Comunicação – e as tecnologias e modelo Open Source na estratégia de crescimento e desenvolvimento de Portugal nos próximos anos, e no contexto da campanha para as eleições legislativas de Outubro, a <strong>ESOP – Associação de Empresas de Software Open Source Portuguesas – está a organizar, em parceria com o MOSS (Mestrado em Software de Código Aberto)</strong> do ISCTE-IUL, a sessão de <span style="text-decoration: underline;">discussão pública</span><span style="text-decoration: underline;"> “POS2020 – </span><span style="text-decoration: underline;">Portugal Open Source 2020</span><span style="text-decoration: underline;">”</span>. O debate, agendado para o <span style="text-decoration: underline;">dia 1</span><span style="text-decoration: underline;">6</span><span style="text-decoration: underline;"> de Setembro, pelas </span><span style="text-decoration: underline;">17h00,</span><span style="text-decoration: underline;"> no ISCTE-</span><span style="text-decoration: underline;">IUL</span>, contará com a presença de representantes dos partidos políticos com assento parlamentar.<br> </span></span></p><p style="margin-bottom: 0.25cm; font-weight: normal; line-height: 100%;" align="justify"><span style="font-family: sans-serif;"><span style="font-size: medium;">A participação é livre mas sujeita a inscrição prévia.<br> </span></span></p><p><span style="font-size: medium;">Para informação adicional visitem a página <a href="http://www.esop.pt/Default/pt/Destaques/Noticia?A=49" target="_blank">http://www.esop.pt/Default/pt/Destaques/Noticia?A=49</a><br> <br> Para fazer a inscrição, enviem nome, entidade e contacto para <a href="mailto:assessora@esop.pt" target="_blank">assessora@esop.pt</a></span></p>
