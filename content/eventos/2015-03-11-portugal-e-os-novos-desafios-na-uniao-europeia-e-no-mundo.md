---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 293
  event:
    location: 
    site:
      title: ''
      url: http://www.fd.uc.pt/igc/ficha.php
    date:
      start: 2015-03-13 00:00:00.000000000 +00:00
      finish: 2015-03-13 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Portugal e os (Novos) Desafios na União Europeia e no Mundo
created: 1426115371
date: 2015-03-11
aliases:
- "/evento/293/"
- "/node/293/"
---
<p>Inclui apresentação e debate sobre o TTIP.</p>
