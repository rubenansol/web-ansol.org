---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa, Museu da Farmácia
  event_site:
  - event_site_url: https://acessocultura.org/acesso-aberto/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2020-03-25 00:00:00.000000000 +00:00
    event_start_value2: 2020-03-25 00:00:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 733
layout: evento
title: 'Acesso aberto: acesso universal às colecções de museus e arquivos'
created: 1581022900
date: 2020-02-06
---
<p>(evento adiado, novas datas a anunciar)</p>
