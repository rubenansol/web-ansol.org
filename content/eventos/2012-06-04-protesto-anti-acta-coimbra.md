---
categories: []
metadata:
  node_id: 78
  event:
    location: Praça da República, Coimbra
    site:
      title: ''
      url: https://www.facebook.com/#!/events/284963381600253/
    date:
      start: 2012-06-09 15:00:00.000000000 +01:00
      finish: 2012-06-09 15:00:00.000000000 +01:00
    map: {}
layout: evento
title: Protesto anti-ACTA - Coimbra
created: 1338803188
date: 2012-06-04
aliases:
- "/evento/78/"
- "/node/78/"
---
<p>(veja tamb&eacute;m informa&ccedil;&atilde;o sobre o <a href="https://ansol.org/acta/protesto-20120609">protesto em Lisboa</a>)</p>
