---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: https://sobre.arquivo.pt/pt/wdpd2019/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-11-07 00:00:00.000000000 +00:00
    event_start_value2: 2019-11-07 00:00:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 702
layout: evento
title: Dia Mundial da Preservação Digital
created: 1572193396
date: 2019-10-27
---

