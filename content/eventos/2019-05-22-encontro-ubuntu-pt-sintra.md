---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 676
  event:
    location: Sintra
    site:
      title: ''
      url: https://www.meetup.com/ubuntupt/events/261481532
    date:
      start: 2019-07-25 19:00:00.000000000 +01:00
      finish: 2019-07-25 19:00:00.000000000 +01:00
    map: {}
layout: evento
title: Encontro Ubuntu-pt @ Sintra
created: 1558558258
date: 2019-05-22
aliases:
- "/evento/676/"
- "/node/676/"
---

