---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 270
  event:
    location: Assembleia da República
    site:
      title: ''
      url: http://app.parlamento.pt/webutils/docs/doc.pdf?Path=6148523063446f764c324679626d56304c334e706447567a4c31684a5355786c5a793944543030764d554e425130524d527939485645524252454d7651584a7864576c3262304e7662576c7a633246764c3039795a4756756379426b5a534255636d46695957786f627939485645524252454e664e6935775a47593d&Fich=GTDADC_6.pdf&Inline=true
    date:
      start: 2015-01-28 11:30:00.000000000 +00:00
      finish: 2015-01-29 19:00:00.000000000 +00:00
    map: {}
layout: evento
title: Continuação da apreciação e votação indiciária na especialidade das PL sobre
  Direitos de Autor
created: 1422371392
date: 2015-01-27
aliases:
- "/evento/270/"
- "/node/270/"
---
<p><strong>Grupo de Trabalho - Direito de Autor e Direitos Conexos [PPL 245, 246 e 247/XII/3.ª (GOV)]</strong></p><p>Datas:</p><ul><li>dia 28, 11:30</li><li>dia 29, 18:00</li></ul><p>Continuação da apreciação e votação indiciária na especialidade da:</p><ul><li>Proposta de Lei n.º 245/XII/3.ª (GOV) - "Regula as entidades de gestão coletiva do direito de autor e dos direitos conexos, inclusive quanto ao estabelecimento em território nacional e à livre prestação de serviços das entidades previamente estabelecidas noutro Estado-Membro da União Europeia ou do Espaço Económico Europeu"</li><li>Proposta de Lei n.º 246/XII/3.ª (GOV) - "Procede à segunda alteração à Lei n.º 62/98, de 1 de setembro, que regula o disposto no artigo 82.º do Código do Direito de Autor e dos Direitos Conexos, sobre a compensação equitativa relativa à cópia privada"</li><li>Proposta de Lei n.º 247/XII/3.ª (GOV) - "Transpõe a Diretiva n.º 2012/28/UE, do Parlamento Europeu e do Conselho, de 25 de outubro, relativa a determinadas utilizações permitidas de obras órfãs, e procede à décima alteração ao Código do Direito de Autor e dos Direitos".</li></ul>
