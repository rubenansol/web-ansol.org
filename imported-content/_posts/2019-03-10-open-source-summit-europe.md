---
categories:
- ossummit
- linux foundation
- open source
metadata:
  event_location:
  - event_location_value: Lyon Convention Centre, Lyon, France
  event_site:
  - event_site_url: https://events.linuxfoundation.org/events/open-source-summit-europe-2019/
    event_site_title: Open Source Summit Europe
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-10-28 00:00:00.000000000 +00:00
    event_start_value2: 2019-10-30 00:00:00.000000000 +00:00
  mapa:
  - mapa_geom: !binary |-
      AQEAAAD///+/SmkTQMOdVjZw5EZA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.45784674446385e2
    mapa_lon: !ruby/object:BigDecimal 27:0.4852824211121e1
    mapa_left: !ruby/object:BigDecimal 27:0.4852824211121e1
    mapa_top: !ruby/object:BigDecimal 27:0.45784674446385e2
    mapa_right: !ruby/object:BigDecimal 27:0.4852824211121e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.45784674446385e2
    mapa_geohash: u05kqqvenczvruxv
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 318
  - tags_tid: 319
  - tags_tid: 127
  node_id: 660
layout: evento
title: Open Source Summit Europe
created: 1552181473
date: 2019-03-10
---
<p>Open Source Summit Europe (OSSEU) is a conference by the Linux Foundation, providing a place for developers, architects, and other technologists to collaborate and share information. The three-day-event gives industry leaders and the open source community the opportunity to gather for discussions and presentations about the open source ecosystem</p>
