---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://2021.jnation.pt/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-06-15 23:00:00.000000000 +01:00
    event_start_value2: 2021-06-15 23:00:00.000000000 +01:00
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 802
layout: evento
title: JNation 2021 Online Live Stream
created: 1621718863
date: 2021-05-22
---
<p>JNation's Conference 4th edition is ONLINE and FREE to attend on the 16th of June.</p>
