---
categories:
- open source
- comercial fair
metadata:
  event_location:
  - event_location_value: La Nave Madrid, Madrid, Espanha
  event_site:
  - event_site_url: https://openexpoeurope.com/oe2019
    event_site_title: OpenExpo Europe 2019
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-06-19 23:00:00.000000000 +01:00
    event_start_value2: 2019-06-19 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: !binary |-
      AQEAAAD///+fCpINwKUgDYF4LERA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.40347427493486e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.3696309328079e1
    mapa_left: !ruby/object:BigDecimal 27:-0.3696309328079e1
    mapa_top: !ruby/object:BigDecimal 27:0.40347427493486e2
    mapa_right: !ruby/object:BigDecimal 27:-0.3696309328079e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.40347427493486e2
    mapa_geohash: ezjmech2vc85bn24
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 127
  - tags_tid: 317
  node_id: 659
layout: evento
title: OpenExpo Europe 2019
created: 1552181076
date: 2019-03-10
---
<div class="six columns"><div class="gdlr-item gdlr-content-item"><h4 style="text-align: justify;"><strong>OpenExpo Europe</strong> is the main professional Fair and Congress about Open Source, <strong>Free Software and Open World Economy (Open Data and Open Innovation)</strong> in Europe.</h4><h4 style="text-align: justify;">OpenExpo is the <strong>essential event for the technology and business leaders</strong> looking to develop, invest and implement <strong>Open Source technologies</strong>. OpenExpo is the<strong> meeting point of the sector</strong> for companies and entrepreneurs, engineers, designers, analysts, digital managers, developers and business managers among others, where companies <strong>expand their network of contacts, generate business and promote their services</strong>. The main suppliers of the sector will show their technological solutions for all types of companies. A unique showcase with the vision of experts and key market players. The aim of OpenExpo Europe is to <strong>share, present, discover and evaluate the Open Source solutions</strong> and the trends within the industry. Check out what’s new within the open technology world. Don’t miss the chance to discover the last trends to promote your business.</h4></div></div>
