---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 514
  event:
    location: Lisboa
    site:
      title: ''
      url: https://websummit.com/fullstk
    date:
      start: 2017-11-06 00:00:00.000000000 +00:00
      finish: 2017-11-09 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Web Summit - FullSTK
created: 1501966885
date: 2017-08-05
aliases:
- "/evento/514/"
- "/node/514/"
---
<p>Tal como no ano passado, na Web Summit de 2017, a decorrer em Lisboa, haverá a conferência "FullSTK", onde um dos seis temas será o "Open Source".</p>
