---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 780
  event:
    location: Online
    site:
      title: ''
      url: https://www.swforum.eu/news-events/events/first-swforumeu-workshop-trustworthy-software-and-open-source
    date:
      start: 2021-03-23 00:00:00.000000000 +00:00
      finish: 2021-03-25 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: First SWForum.eu Workshop on Trustworthy Software and Open Source
created: 1613850507
date: 2021-02-20
aliases:
- "/evento/780/"
- "/node/780/"
---

