---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 186
  event:
    location: Congresso do Brasil, Brasilia
    site:
      title: 
      url: 
    date:
      start: 2003-08-18 00:00:00.000000000 +01:00
      finish: 2003-08-22 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Semana Software Livre no Legislativo
created: 1371948838
date: 2013-06-23
aliases:
- "/evento/186/"
- "/node/186/"
---
<p>Jaime Villate (ANSOL) fez uma apresentação sobre "Uma Visão Institucional Internacional sobre o Software Livre" no dia 19.</p>
