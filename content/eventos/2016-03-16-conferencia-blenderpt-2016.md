---
categories:
- blender
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 161
  node_id: 405
  event:
    location: Instituto Superior de Engenharia de Coimbra, Rua Pedro Nunes, 3030-199
      Coimbra
    site:
      title: Conferência BlenderPT
      url: http://www.conferencia.problender.pt/
    date:
      start: 2016-04-15 09:00:00.000000000 +01:00
      finish: 2016-04-16 21:00:00.000000000 +01:00
    map: {}
layout: evento
title: Conferência BlenderPT 2016
created: 1458143150
date: 2016-03-16
aliases:
- "/evento/405/"
- "/node/405/"
---

