---
categories: []
metadata:
  event:
    location: Online
    site:
      url: https://2021.foss4g.org/
    date:
      start: 2021-09-27 00:00:00.000000000 +01:00
      finish: 2021-10-02 00:00:00.000000000 +01:00
title: FOSS4G 2021
aliases:
- "/evento/811/"
- "/node/811/"
---
