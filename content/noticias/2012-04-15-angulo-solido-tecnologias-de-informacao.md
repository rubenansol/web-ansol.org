---
categories:
- consultoria
- suporte
metadata:
  email:
  - email_email: geral@angulosolido.pt
  servicos:
  - servicos_tid: 7
  - servicos_tid: 2
  site:
  - site_url: http://angulosolido.pt
    site_title: http://angulosolido.pt
    site_attributes: a:0:{}
  node_id: 37
layout: servicos
title: Angulo Sólido - Tecnologias de Informação
created: 1334498869
date: 2012-04-15
aliases:
- "/node/37/"
- "/servicos/37/"
---
<p><strong>Estabilidade, Seguran&ccedil;a e Pre&ccedil;o Competitivo</strong></p>
<ul>
	<li>
		Desktops Linux - com verdadeira garantia de qualidade. Toda a funcionalidade esperada para um PC de escrit&oacute;rio
		<ul>
			<li>
				Desktops especializados para CAD.</li>
			<li>
				Arquitecturas thin client - recupera&ccedil;&atilde;o de computadores em fim de vida.</li>
		</ul>
	</li>
	<li>
		Servidores Linux.
		<ul>
			<li>
				Partilha de ficheiros</li>
			<li>
				Web</li>
			<li>
				Email com filtragem de SPAM, anti-v&iacute;rus e acesso WebMail</li>
			<li>
				Fax Gateway</li>
			<li>
				Proxy acesso Internet</li>
		</ul>
	</li>
	<li>
		Forma&ccedil;&atilde;o e manuten&ccedil;&atilde;o de sistemas.</li>
</ul>
