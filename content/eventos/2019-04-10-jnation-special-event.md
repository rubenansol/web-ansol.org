---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 665
  event:
    location: Porto
    site:
      title: ''
      url: https://www.meetup.com/pt-jug/events/260472381/
    date:
      start: 2019-04-15 00:00:00.000000000 +01:00
      finish: 2019-04-15 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: JNation special event
created: 1554930419
date: 2019-04-10
aliases:
- "/evento/665/"
- "/node/665/"
---

