---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 249
  event:
    location: 
    site:
      title: ''
      url: http://www.hfday.org/
    date:
      start: 2015-01-17 00:00:00.000000000 +00:00
      finish: 2015-01-17 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Hardware Freedom Day
created: 1416578525
date: 2014-11-21
aliases:
- "/evento/249/"
- "/node/249/"
---
<p><img src="http://www.hfday.org/countdown/banner1-UTC-1-en.png" height="154" width="154"></p><p>A ANSOL está a estudar qual a melhor forma de marcar esta data. Tens uma ideia? Queres ajudar? Contacta-nos!</p>
