---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 734
  event:
    location: Coimbra
    site:
      title: ''
      url: http://pcdcoimbra.dei.uc.pt
    date:
      start: 2020-04-01 00:00:00.000000000 +01:00
      finish: 2020-04-01 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Processing Community Day
created: 1583595522
date: 2020-03-07
aliases:
- "/evento/734/"
- "/node/734/"
---

