---
categories: []
metadata:
  event_location:
  - event_location_value: 'Lisbon Congress Center '
  event_site:
  - event_site_url: https://www.cloudbees.com/devops-world/lisbon
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-12-02 00:00:00.000000000 +00:00
    event_start_value2: 2019-12-05 00:00:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 645
layout: evento
title: Jenkins World 2019 Europe
created: 1549209609
date: 2019-02-03
---

