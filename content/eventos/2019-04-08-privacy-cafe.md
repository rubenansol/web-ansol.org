---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 663
  event:
    location: Biblioteca dos Corucheus | Rua Alberto de Oliveira, 1700-019 Lisboa,
      Portugal
    site:
      title: ''
      url: https://privacylx.org/events/privacy-cafe.2019-04/
    date:
      start: 2019-04-12 16:00:00.000000000 +01:00
      finish: 2019-04-12 16:00:00.000000000 +01:00
    map: {}
layout: evento
title: Privacy cafe
created: 1554720023
date: 2019-04-08
aliases:
- "/evento/663/"
- "/node/663/"
---
<center>Um encontro informal para aprender e trocar ideas sobre privacidade digital.</center>
