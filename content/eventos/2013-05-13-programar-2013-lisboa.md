---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 162
  event:
    location: Parque das Nações, Lisboa
    site:
      title: ''
      url: http://evento.portugal-a-programar.pt/
    date:
      start: 2013-05-25 09:00:00.000000000 +01:00
      finish: 2013-05-25 18:30:00.000000000 +01:00
    map: {}
layout: evento
title: Programar 2013 Lisboa
created: 1368467177
date: 2013-05-13
aliases:
- "/evento/162/"
- "/node/162/"
---
<p>Inclui apresentação "Desenvolvimento rápido de sítios web com personalização de Joomla" por Rui Guimarães, membro da Direcção da ANSOL.</p>
