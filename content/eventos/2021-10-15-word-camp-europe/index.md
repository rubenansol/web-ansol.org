---
metadata:
  event:
    location: Porto, Portugal
    site:
      url: https://europe.wordcamp.org/2022/
    date:
      start: 2022-06-02
      finish: 2022-06-04
layout: evento
title: Word Camp Europe
---
