---
categories:
- lan party
- jogos
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 118
  - tags_tid: 144
  node_id: 372
  event:
    location: Moita, Portugal
    site:
      title: LanParty da Moita
      url: http://www.lanpartymoita.com/
    date:
      start: 2015-12-04 00:00:00.000000000 +00:00
      finish: 2015-12-06 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Lan Party da Moita
created: 1445803509
date: 2015-10-25
aliases:
- "/evento/372/"
- "/node/372/"
---
<p class="style1">Este ano marca o renascimento da Lan Party Moita.</p><p class="style2">- O que é que isto significa?</p><p>A LPM é o evento que marcou o concelho da moita durante diversos anos. Infelizmente, por falta de tempo da equipa organizadora da altura, o projecto teve o seu fim em 2009, apesar da sua sempre grande adesão, tendo a sua última edição contado com quase 500 pessoas.</p><p>6 anos depois, aqui estamos. A Lan Party Moita vai voltar a criar a sua marca nos quase 300 “Gamers” que se irão encontrar nos dias 4,5 e 6 de Dezembro no Pavilhão Municipal de Exposições na Moita, com a sua versão melhorada: V2.0.</p><p><span class="style2">- A Lan Party Moita tem como principais objetivos:&nbsp;</span><br><br>- Convívio entre participantes;<br>- Levar a que muitos deles conheçam pessoalmente quem lidam no “mundo virtual”;<br>- Troca de Experiencias;<br>- Promover a competição Amigável;<br>- Promover empresas/entidades junto do seu público-alvo.</p><p class="style2">- Quem somos ?</p><p>A nossa equipa é constituída por um grupo de jovens, de diversas idades, com já 10 anos de experiência na organização deste tipo de eventos, e com muita vontade de continuar a organizar Lan Parties para a nossa comunidade de gamers. Gostamos de receber sempre gente nova, esforçada, empenhada, motivada e a cima de tudo simpática e divertida, na nossa equipa. Passamos a nossa experiência e o nosso conhecimento na área aos mais novos e encutimos-lhes o gosto pelo que fazemos.</p><p class="style2">- Porque o fazemos?</p><p>Resumindo: Amor à camisola. Crescemos com este tipo de eventos, que ao longo dos anos têm vindo a extinguir e, fazendo parte da nossa vida não queremos deixar de promover esta experiência para que todos possam experimentar. Gostamos de sentir que as pessoas gostam de conviver com quem jogam e dispendem tempo online, queremos promover o convívio, a amizade, a competitividade, o conhecimento. Apreciamos a cima de tudo o facto de as pessoas gostarem de estar no evento e o ambiente de confraternidade que se faz sentir. Corre nas veias !</p><p class="style2">- Como o fazemos?</p><p>Dada a dimensão do evento e de toda a infraestrutura necessária para o mesmo, só se torna possível promover esta experiência, que nos dias de hoje tem vindo a escassear, se contarmos com o envolvimento de diversas empresas para o apoio a nível de prémios, equipamento e divulgação e é nesse sentido que nos dirigimos ás empresas, no sentido de nos apoiarem e receberem de volta reconhecimento e gratidão.</p><p>Para além dos nossos patrocinadores contamos também com muito apoio e sacrifício da nossa equipa, amigos e familiares.</p>
