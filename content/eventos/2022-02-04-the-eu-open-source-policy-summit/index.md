---
categories:
- assembleia geral
layout: evento
title: The EU Open Source Policy Summit
metadata:
  event:
    date:
      start: 2022-02-04 08:00:00 +00:00
      finish: 2022-02-04 17:00:00 +00:00
    location: Online
    site:
      url: https://summit.openforumeurope.org/
---

Open Source & the Grand Challenges

The grand challenges cannot be tackled by any company or country alone. Climate
change, current and future pandemics, digital autonomy, market concentration,
and reaching the UN’s Sustainable Development Goals are all pressing issues
that underscore the need for new institutional set-ups. But the world’s
stakeholders have not been idle: modes of collaboration at a completely new
scale are needed to find solutions to complex problems. This takes place in a
day-to-day reality that is increasingly digital, where software is ubiquitous,
and while we see increased concerns over autonomy in a multi-polar world.

Open Technologies and Open Source are at the heart of this new reality and are
a source for optimism. ‘Open’ lowers the barriers to competition, delivers
collaborative innovation, increases welfare, while letting end-users maintain
control and autonomy. Open and collaborative digital innovation is no longer a
theoretical concept. It is the modus operandi of companies and countries that
engage at the cutting edge of the technological and societal innovation needed
to tackle the grand challenges.

2022 will be the eighth year OpenForum Europe hosts its Open Source policy
event right before FOSDEM. This will be a fully online event. What started as a
10-person meeting of minds on the then-niche subject of Open Source policy is
now a successful annual gathering of policymakers, Open Source businesses,
community members, developers, foundations and those involved with diverse
digital policy subjects. Take a look at the sessions and recordings from the
[2021 Summit](https://openforumeurope.org/event/policy-summit-2021/).
