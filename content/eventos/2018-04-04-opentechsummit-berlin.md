---
categories:
- open source
- free software
- germany
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 127
  - tags_tid: 122
  - tags_tid: 258
  node_id: 577
  event:
    location: Freiland Potsdam, Friedrich-Engels-Str. 22, Potsdam, Berlim, Alemanha
    site:
      title: Open Tech Summit
      url: https://opentechsummit.net/
    date:
      start: 2018-04-10 00:00:00.000000000 +01:00
      finish: 2018-04-10 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: OpenTechSummit Berlin
created: 1522858525
date: 2018-04-04
aliases:
- "/evento/577/"
- "/node/577/"
---
<p>Um dia de tecnologias abertas para si e para a sua família.</p><div class="col-sm-6"><span class="lead color-heading">OpenTechSummit</span><p>Der OpenTechSummit bringt die spannendsten Ideen und Macher der Open-Technology-Community in Postdam zusammen. Themen reichen von Open Hardware, über Enzyklopädien des freien Wissens, Software-Entwicklung und freie Netze bis hin zu Fragen&nbsp;<span>ü</span>ber Copyright&nbsp;und Patente für Firmengründer. Für Kinder und Technik-Begeisterte gibt es Workshops zum Selberbauen. Der erste OpenTechSummit fand bereits 2009 in Taiwan statt und ist das 3.Mal in Deutschland zu Gast.</p></div>
