---
categories: []
metadata:
  event_location:
  - event_location_value: Polo das Indústrias Criativas (UPTEC PINC), Praça Coronel
      Pacheco, 2, Porto
  event_site:
  - event_site_url: https://direitosdigitais.pt/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-09-23 23:00:00.000000000 +01:00
    event_start_value2: 2016-09-23 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 449
layout: evento
title: Direitos Digitais - Reunião Constituinte
created: 1473238790
date: 2016-09-07
---
<p>Reunião Constituinte do movimento "Direitos Digitais", a 24 de Setembro no Porto.</p><p><img src="https://pbs.twimg.com/media/CrQeriFXYAA6pfP.jpg:large" height="512" width="1024"></p>
