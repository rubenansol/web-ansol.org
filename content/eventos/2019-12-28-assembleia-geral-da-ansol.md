---
categories: []
metadata:
  mapa:
  - mapa_geom: !binary |-
      AQEAAAAAPAAcmTkhwBS/uihqkkRA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.41143864718613e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.8612496256856e1
    mapa_left: !ruby/object:BigDecimal 27:-0.8612496256856e1
    mapa_top: !ruby/object:BigDecimal 27:0.41143864718613e2
    mapa_right: !ruby/object:BigDecimal 27:-0.8612496256856e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.41143864718613e2
    mapa_geohash: ez3fh4086k3349tm
  slide:
  - slide_value: 0
  node_id: 722
  event:
    location: FAJDP - Casa das Associações
    site:
      title: 
      url: 
    date:
      start: 2020-02-29 14:00:00.000000000 +00:00
      finish: 2020-02-29 14:00:00.000000000 +00:00
    map: {}
layout: evento
title: Assembleia Geral da ANSOL
created: 1577561890
date: 2019-12-28
aliases:
- "/AG2020/"
- "/evento/722/"
- "/node/722/"
---
<p>Convocam-se todos os sócios da ANSOL para a Assembleia Geral Eleitoral que terá lugar no dia 29 de Fevereiro de 2020 pelas 14 horas na Casa das Associações, Rua Mouzinho da Silveira, 234/6/8 4050-417 PORTO, com a seguinte ordem de trabalhos:</p><ol style="list-style-type: lower-alpha;">Aprovação do Relatório e Contas de 2019;</ol><ol>Eleição dos Órgãos Sociais 2020/2021;</ol><ol>Apresentação do programa da lista vencedora;</ol><ol>Outros assuntos.</ol><p>Se à hora marcada não estiverem presentes, pelo menos, metade dos associados a Assembleia Geral reunirá, em 2ª convocatória, no mesmo local e passados 30 minutos, com qualquer número de presenças.</p><p>A FAJDP - Casa das Associações situa-se na zona do centro histórico do Porto.</p><p>Foi apresentada uma candidatura, feita em carta que deu entrada na sede social da Associação até dia 29 de Janeiro de 2020:</p><p><strong>Lista A</strong></p><p><em><strong>Direcção:</strong></em>Presidente - Tiago Miguel Feiteiro Carrondo<br>Vice-presidente - Marcos Daniel Marado Torres<br>Tesoureiro - Octávio Filipe da Mota Pereira Gonçalves<br>Vogal - Daniel Sousa<br>Vogal - Rúben Jaime Alegria Leote Mendes<br><br><em><strong>Mesa da</strong><strong> Assembleia:</strong><strong><br></strong></em>Presidente - Rui Miguel Silva Seabra<br>1º Secretário - André Isidoro Fernandes Esteves<br>2º Secretário - Tiago Filipe da Costa Gomes Policarpo<br><br><em><strong>Conselho</strong><strong> Fiscal:</strong><strong><br></strong></em>Presidente - Jaime dos Santos Pereira<br>1º Secretário - Sandra Fernandes<br>2º Secretário - Diogo Miguel Conceição Figueira</p>
