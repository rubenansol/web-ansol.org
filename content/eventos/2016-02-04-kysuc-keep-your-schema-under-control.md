---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 402
  event:
    location: Coimbra
    site:
      title: ''
      url: http://www.meetup.com/Coimbra-JUG/events/228223006/
    date:
      start: 2016-02-04 18:15:00.000000000 +00:00
      finish: 2016-02-04 18:15:00.000000000 +00:00
    map: {}
layout: evento
title: KYSUC - Keep Your Schema Under Control
created: 1454600034
date: 2016-02-04
aliases:
- "/evento/402/"
- "/node/402/"
---
<p>Na 13ª edição dos encontros Coimbra JUG vamos ter o prazer de contar com o Nuno Alves, um membro do grupo que se disponibilizou para fazer uma apresentação sobre versionamento de Bases de Dados.</p><p><span>É comum termos a preocupação de versionar o código, mas ninguém se preocupa com o que acontece com a Base de Dados. Não deveria ser tratada como o código da aplicação? O Nuno, vai responder a esta e muitas outras questões e mostrar como podemos&nbsp;versionar uma Base de Dados, como fazemos com o código.</span></p><p><span>Não deixem de aparecer!</span></p>
