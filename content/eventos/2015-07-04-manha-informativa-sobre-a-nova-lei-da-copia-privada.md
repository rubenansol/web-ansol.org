---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 332
  event:
    location: 
    site:
      title: ''
      url: http://2.bp.blogspot.com/-To9GBhRdHN4/VYxs3n694bI/AAAAAAAADAY/hnVpt_F_8jc/s1600/25-06-15%2BNova%2BLei%2Bda%2BCopia%2BPrivada.png
    date:
      start: 2015-07-13 00:00:00.000000000 +01:00
      finish: 2015-07-13 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Manhã Informativa sobre a Nova Lei da Cópia Privada
created: 1436047209
date: 2015-07-04
aliases:
- "/evento/332/"
- "/node/332/"
---
<p>Numa altura em que a GDA - entidade que faz a gestão dos Direitos de Artistas - está a levar a cabo a campanha "Fair Internet For Performers" e que, em Portugal, outras vozes se levantam contra a forma como a lei "trata" o consumidor, a VdA Advogados vai realizar uma Manhã Informativa sobre a Nova Lei da Cópia Privada.<br> <br> A sessão vai ter lugar na sede da VdA, na Av.Duarte Pacheco, n.º26, em Lisboa, no dia 13 de julho, entre as 9h15 e as 12h00. Pode obter mais<br> informações através do <a href="mailto:e-mailsmc@vda.pt" target="_blank">e-mailsmc@vda.pt</a>.<br> <br>Questões como a pirataria e os direitos digitais tornam-se cada vez mais relevantes na medida em que o Spotify e o Youtube estão definitivamente instalados no dia-a-dia do consumidor de música.</p>
