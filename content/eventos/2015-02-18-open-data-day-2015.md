---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 285
  event:
    location: Pavilhão-Jardim do Pólo de Indústrias Criativas, Porto
    site:
      title: ''
      url: http://www.transparenciahackday.org/2015/02/open-data-day-2015/
    date:
      start: 2015-02-21 14:30:00.000000000 +00:00
      finish: 2015-02-21 14:30:00.000000000 +00:00
    map: {}
layout: evento
title: Open Data Day 2015
created: 1424269785
date: 2015-02-18
aliases:
- "/evento/285/"
- "/node/285/"
---
<p>Sábado, 21 de fevereiro, comemoramos mais um <a href="http://opendataday.org/" target="_blank" title="International Open Data Day">Open Data Day</a> com uma edição especial do Transparência Hackday. Para participar só tens de te inscrever <a href="https://www.eventbrite.com/e/open-data-day-porto-tickets-15237998263" target="_blank" title="Bilhetes - EventBrite"><strong>aqui</strong></a>.</p><p>O <a href="http://opendataday.org/" target="_blank" title="International Open Data Day">Open Data Day</a> é um evento internacional dedicado ao ativismo pelos <a href="https://pt.wikipedia.org/wiki/Dados_abertos" target="_blank" title="Dados Abertos (Wikipedia)">Dados Abertos</a>. Em vários locais do mundo, colectivos e organizações dedicados ao trabalho nesta área juntam-se para assinalar a data com iniciativas locais.</p>
