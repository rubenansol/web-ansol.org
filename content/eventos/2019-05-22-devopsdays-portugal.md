---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 672
  event:
    location: 
    site:
      title: ''
      url: https://devopsdays.org/events/2019-portugal/welcome/
    date:
      start: 2019-06-03 00:00:00.000000000 +01:00
      finish: 2019-06-04 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: devopsdays Portugal
created: 1558557773
date: 2019-05-22
aliases:
- "/evento/672/"
- "/node/672/"
---

