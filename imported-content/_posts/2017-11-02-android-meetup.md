---
categories:
- android
metadata:
  event_location:
  - event_location_value: SPMS - Serviços Partilhados do Ministério da Saúde
  event_site:
  - event_site_url: https://www.meetup.com/Android-LX/events/244754524
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2017-11-23 19:00:00.000000000 +00:00
    event_start_value2: 2017-11-23 19:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 78
  node_id: 525
layout: evento
title: Android Meetup
created: 1509657603
date: 2017-11-02
---

