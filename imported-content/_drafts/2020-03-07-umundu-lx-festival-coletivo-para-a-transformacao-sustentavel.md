---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: https://www.umundu.pt/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2020-10-08 23:00:00.000000000 +01:00
    event_start_value2: 2020-10-16 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 735
layout: evento
title: Umundu Lx - Festival Coletivo para a Transformação Sustentável
created: 1583597348
date: 2020-03-07
---

