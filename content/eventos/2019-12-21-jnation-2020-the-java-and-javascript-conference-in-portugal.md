---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 721
  event:
    location: online
    site:
      title: ''
      url: https://2020.jnation.pt/
    date:
      start: 2020-06-02 00:00:00.000000000 +01:00
      finish: 2020-06-03 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: JNation 2020 - The Java and Javascript Conference in Portugal
created: 1576956413
date: 2019-12-21
aliases:
- "/evento/721/"
- "/node/721/"
---

