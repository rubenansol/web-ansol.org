---
excerpt: "<p>Pode inscrever-se na associa&ccedil;&atilde;o qualquer pessoa (portuguesa
  ou estrangeira, moradora em Portugal ou no estrangeiro) que possa contribuir para
  a prossecu&ccedil;&atilde;o dos objectivos da Associa&ccedil;&atilde;o.</p>\r\n<p>O
  primeiro passo para a inscri&ccedil;&atilde;o consiste em tomar conhecimento e aceitar
  os <a href=\"estatutos\">estatutos</a>. O <a href=\"regulamento_interno\">regulamento
  interno</a> tamb&eacute;m dever&aacute; ser lido e aceite. Não há j&oacute;ia de
  inscri&ccedil;&atilde;o, e a quota anual 30 euros (12 euros no caso de estudantes,
  desempregados e reformados).</p>\r\n"
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 18
layout: page
title: Inscrição na ANSOL
created: 1333266103
date: 2012-04-01
---
<p>Pode inscrever-se na associação qualquer pessoa (portuguesa ou estrangeira, moradora em Portugal ou no estrangeiro) que possa contribuir para a prossecução dos objectivos da Associação. Não são aceites inscrições de empresas, mas um ou várias pessoas da empresa podem fazer a sua inscrição individual.</p><p>O primeiro passo para a inscrição consiste em tomar conhecimento e aceitar os <a href="estatutos">estatutos</a>. O <a href="regulamento_interno">regulamento interno</a> também deverá ser lido e aceite. A jóia de inscrição é actualmente inexistente (0 euro), e a quota anual 30 euros (12 euros no caso de estudantes, desempregados e reformados).</p><p>O pagamento da quota pode ser feito pessoalmente, por cheque, ou transferência bancária para o IBAN <strong>PT50&nbsp;0035 2178 00027478430 14</strong> .<strong><br></strong></p><p>A inscrição é feita através do preenchimento do <a href="https://form.ansol.org/forms/inscricao_socios_form.php">formulário</a> para o efeito.</p><p><span style="font-size: 13.008px; line-height: 1.538em;"><span style="font-size: 13.008px; line-height: 1.538em;">Após aprovação por parte da direcção, o candidato deve pagar o valor da jóia e da quota, passando a ser sócio a partir do momento em que a ANSOL receba o comprovativo de pagamento. Este comprovativo deve ser enviado num email para&nbsp;</span><a href="mailto:direccao@ansol.org">direccao@ansol.org</a><span style="font-size: 13.008px; line-height: 1.538em;">.</span></span></p>
