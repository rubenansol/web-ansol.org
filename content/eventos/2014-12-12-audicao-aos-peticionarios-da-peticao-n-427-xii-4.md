---
categories:
- cópia privada
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 11
  node_id: 256
  event:
    location: Assembleia da República
    site:
      title: ''
      url: http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalhePeticao.aspx?BID=12552
    date:
      start: 2014-12-17 14:00:00.000000000 +00:00
      finish: 2014-12-17 14:00:00.000000000 +00:00
    map: {}
layout: evento
title: Audição aos peticionários da petição Nº 427/XII/4ª
created: 1418404748
date: 2014-12-12
aliases:
- "/evento/256/"
- "/node/256/"
---

