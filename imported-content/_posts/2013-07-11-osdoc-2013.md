---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: http://osdoc2013.wordpress.com/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-07-10 23:00:00.000000000 +01:00
    event_start_value2: 2013-07-10 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 138
layout: evento
title: OSDOC 2013
created: 1373542635
date: 2013-07-11
---
<p>O OSDOC (Open Source and Design Of Communication) é um workshop onde investigadores e praticantes irão trocar informação em áreas de relevância para o design da comunicação, processos, métodos e tecnologias para comunicar e desenhar artefactos de comunicação tal como documentos impressos, texto online e aplicações hipermedia.</p><p>O Comité do Programa conta com a presença de dois representantes da ANSOL.</p><p>&nbsp;</p><p>&nbsp;</p>
