---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 636
  event:
    location: Sintra
    site:
      title: ''
      url: https://www.meetup.com/ubuntupt/events/255537842/
    date:
      start: 2018-12-06 18:30:00.000000000 +00:00
      finish: 2018-12-06 18:30:00.000000000 +00:00
    map: {}
layout: evento
title: Containers LXD
created: 1543102390
date: 2018-11-24
aliases:
- "/evento/636/"
- "/node/636/"
---
<p>Esta apresentação sobre LXD irá explicar o que é o LXD, quais as tecnologias que o LXD tem como base e qual é a ligação com LXC.<br> Como poderão instalar e operar um servidor a correr em LXD e os containers que neles correm.<br> Várias opções em como podem pôr os vossos containers a aceder à Internet e em como podem providenciar serviços na rede a correr nos vossos containers.<br> Serão também apresentados outros tópicos e informações que podem fazer uso para tirar o máximo partido sobre LXD.</p><p>Quem vai falar?</p><p>David Negreira – é um Administrador de Sistemas Linux por hobby e por profissão, iniciando a sua ‘carreira’ pelo mundo open source com a instalação do Slackware 10.1. Com uma carreira bastante diversa em que começou a trabalhar em redes empresariais em bancos até websites com alto tráfego, o Linux sempre foi o caminho preferido para crescer profissionalmente, mantendo sempre contacto com a comunidade Open-source, escrevendo pequenos patches e bug reports.</p><p>Nota: Os lugares são limitados, confirma a tua presença em https://www.meetup.com/ubuntupt/events/255537842/</p><p>Mais Informações:<br> Chalet 12<br> Av. Dr. Desidério Cambournac, 12 – 2710-553 Sintra<br> (2 min a pé da estaçao de comboios da portela de Sintra)<br> https://www.openstreetmap.org/#map=19/38.80237/-9.38091</p>
