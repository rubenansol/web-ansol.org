---
categories: []
metadata:
  node_id: 105
layout: page
title: Software Livre na Administração Pública
created: 1352849811
date: 2012-11-13
aliases:
- "/node/105/"
- "/page/105/"
- "/politica/ap/"
---
<p>O Software Livre, pela sua natureza, apresenta in&uacute;meras vantagens sociais, legais, pol&iacute;ticas e econ&oacute;micas sobre as alternativas propriet&aacute;rias para o seu uso alargado na Administra&ccedil;&atilde;o P&uacute;blica.</p>
<p>Software Livre &eacute; uma designa&ccedil;&atilde;o que se aplica ao conjunto de programas e sistemas operativos que d&atilde;o a cada utilizador 4 liberdades:</p>
<ul>
	<li>
		Liberdade 0 - A liberdade de executar o software, para qualquer uso.</li>
	<li>
		Liberdade 1 - A liberdade de estudar o funcionamento de um programa e de adapt&aacute;-lo &agrave;s suas necessidades.</li>
	<li>
		Liberdade 2 - A liberdade de redistribuir c&oacute;pias.</li>
	<li>
		Liberdade 3 - A liberdade de melhorarem o programa e de tornar as vossas modifica&ccedil;&otilde;es p&uacute;blicas de modo que a comunidade inteira beneficie da melhoria.</li>
</ul>
<p>Software n&atilde;o-livre ou propriet&aacute;rio &eacute; todo aquele que n&atilde;o d&aacute; aos seus utilizadores estas 4 liberdades. Estas liberdades n&atilde;o representam qualquer obrigatoriedade, nem de uso, nem de distribui&ccedil;&atilde;o do software em causa para o seu utilizador.</p>
<p>Estas 4 liberdades t&ecirc;m impactos diversos ao n&iacute;vel legal, pol&iacute;tico, social e econ&oacute;mico.</p>
<h2>
	Legal</h2>
<p>Existem 3 problemas com que a Administra&ccedil;&atilde;o P&uacute;blica tem de resolver, do ponto de vista legal, no que toca ao seu parque inform&aacute;tica: c&oacute;pia ilegal de software (vulgo &quot;pirataria inform&aacute;tica&quot;), protec&ccedil;&atilde;o de dados pessoais dos cidad&atilde;os e seguran&ccedil;a inform&aacute;tica.</p>
<p>A c&oacute;pia ilegal de software pode ser resolvida, pelo menos parcialmente, pela utiliza&ccedil;&atilde;o de Software Livre, uma vez que este pode ser copiado livremente (Liberdade 2). Assim resolvem-se as situa&ccedil;&otilde;es ilegais existentes hoje, o Estado d&aacute; o exemplo de legalidade que lhe compete e poupa dinheiro e a burocracia necess&aacute;ria para vigiar a utiliza&ccedil;&atilde;o de todos os sistemas na Administra&ccedil;&atilde;o P&uacute;blica.</p>
<p>Em rela&ccedil;&atilde;o &agrave; protec&ccedil;&atilde;o de dados pessoais a situa&ccedil;&atilde;o actual &eacute; cr&iacute;tica. Existem sistemas, como os que gerem os passaportes e as cartas de condu&ccedil;&atilde;o, que mant&ecirc;m dados que chegam ao ponto de incluir imagens das nossas assinaturas. Estes sistemas s&atilde;o implementados em tecnologia de software propriet&aacute;rio que n&atilde;o se sabe como funciona, nem se pode legalmente saber. Desta forma &eacute; imposs&iacute;vel definir quem realmente tem acesso aos dados pessoais que a Administra&ccedil;&atilde;o P&uacute;blica detem. Acrescente-se o facto de as &uacute;ltimas licen&ccedil;as de software de fornecedores como a Microsoft incluirem cla&uacute;sulas que autorizam o acesso aos sistemas por parte do fornecedor ou agentes indicados pelo fornecedor, e temos os sistemas estatais num perfeito desrespeito pela legisla&ccedil;&atilde;o vigente de protec&ccedil;&atilde;o de dados pessoais.</p>
<p>A seguran&ccedil;a inform&aacute;tica, tal como a seguran&ccedil;a f&iacute;sica, n&atilde;o existe em termos absolutos. Os &uacute;nicos sistemas realmente seguros s&atilde;o aqueles que incluem os processos de vigil&acirc;ncia e reac&ccedil;&atilde;o a ataques que permitem evitar e prever ataques, reduzir vulnerabilidades e manter o sistema em funcionamento. No caso do software propriet&aacute;rio o processo de reduzir vulnerabilidades depende do fornecedor, ou seja, quaisquer correc&ccedil;&otilde;es ao software s&atilde;o feitas quando, como e se o fornecedor assim o decidir. Com o Software Livre, a Liberdade 1 garante que a capacidade de fazer essas modifica&ccedil;&otilde;es depende, no pior dos casos, apenas dos recursos t&eacute;cnicos ao dispor do utilizador final, permitindo uma melhoria constante do n&iacute;vel de seguran&ccedil;a dos sistemas.</p>
<h2>
	Pol&iacute;tico</h2>
<p>A n&iacute;vel pol&iacute;tico a vantagem do Software Livre &eacute; a independ&ecirc;ncia de qualquer fornecedor de software. Numa altura em que h&aacute; fornecedores a tentarem impedir a venda de empresas a outras por a empresa a ser vendida usar o seu software (Microsoft vs K-Mart), a capacidade de tomar decis&otilde;es na Administr&ccedil;&atilde;o P&uacute;blica, como reorganiza&ccedil;&otilde;es de institui&ccedil;&otilde;es, pode ser limitada por um &uacute;nico fornecedor de software de que dependam alguns dos sistemas existentes. Por isso &eacute; necess&aacute;rio garantir a independ&ecirc;ncia da Administra&ccedil;&atilde;o P&uacute;blica. Tamb&eacute;m aqui o Software Livre apresenta-se como uma alternativa vi&aacute;vel: a Liberdade 0 e a Liberdade 1 garantem a independ&ecirc;ncia necess&aacute;ria &agrave; gest&atilde;o pol&iacute;tica da Administra&ccedil;&atilde;o P&uacute;blica.</p>
<h2>
	Social</h2>
<p>Desde pequenos que tentamos ensinar os mais novos o valor da partilha, come&ccedil;ando pelos seus brinquedos. Este valor de solidariedade est&aacute; t&atilde;o embrenhado na sociedade portuguesa que est&aacute; presente no artigo 1&ordm; da nossa constitui&ccedil;&atilde;o. No entanto, de alguma forma, racionaliz&aacute;mos as restri&ccedil;&otilde;es que o software propriet&aacute;rio nos imp&otilde;e. Saber que uma pessoa precisa de um determinado software, seja para um trabalho acad&eacute;mico ou mesmo no seu dia a dia tem posto muitas pessoas perante o dilema de fazer uma c&oacute;pia ilegal do software e ajudar o seu amigo(a) ou recusar a ajuda. Os actuais n&uacute;meros de c&oacute;pias ilegais de software em Portugal demonstram bem qual &eacute; a escolha normal neste dilema. Deste ponto de vista o Software Livre, atrav&eacute;s da Liberdade 2, garante a resolu&ccedil;&atilde;o deste dilema uma vez que a c&oacute;pia de Software Livre &eacute; legal. Tendo em conta que uma das origens de c&oacute;pias de software ilegal &eacute; o local de trabalho, a adop&ccedil;&atilde;o de Software Livre pela Administra&ccedil;&atilde;o P&uacute;blica levaria a uma utiliza&ccedil;&atilde;o cada vez maior deste tipo de software pela legisla&ccedil;&atilde;o em geral.</p>
<p>Outra vantagem no plano social &eacute; a evolu&ccedil;&atilde;o cient&iacute;fica e tecnol&oacute;gica. A possibilidade de estudar o Software Livre, sem restri&ccedil;&otilde;es como n&atilde;o trabalhar na &aacute;rea nos pr&oacute;ximos 10 anos, permitem a uma pessoa aprender sem estar limitados pelo que o seu fornecedor lhes decide dizer, uma vez que tem acesso a toda a parte funcional do sistema. Por outro lado, aqueles que querem aplicar tecnologia de ponta, t&ecirc;m para os apoiar um conjunto de ferramentas e programas que poder&atilde;o usar sem se preocuparem com cla&uacute;sulas abusivas, falta de suporte e contratos de licenciamento mut&aacute;veis, que lhes permitir&atilde;o implementar mais depressa este tipo de tecnologia.</p>
<h2>
	Econ&oacute;mico</h2>
<p>A n&iacute;vel econ&oacute;mico o Software Livre tem um impacto interno, no que concerne estritamente &agrave; Administra&ccedil;&atilde;o P&uacute;blica, e externo, no que concerne &agrave; sociedade em geral. A n&iacute;vel interno as raz&otilde;es para a redu&ccedil;&atilde;o de custos s&atilde;o: aumento da competi&ccedil;&atilde;o nos servi&ccedil;os de suporte e manuten&ccedil;&atilde;o de sistemas de informa&ccedil;&atilde;o e elimina&ccedil;&atilde;o de custos de licen&ccedil;as na replica&ccedil;&atilde;o de solu&ccedil;&otilde;es.</p>
<p>O aumento da competi&ccedil;&atilde;o nos servi&ccedil;os de suporte &eacute; poss&iacute;vel, uma vez que a Liberdade 1 garante a qualquer empresa que se encontre no mercado pode aprender o suficiente sobre um sistema de Software Livre para oferecer os seus servi&ccedil;os de suporte, deixando assim a administra&ccedil;&atilde;o p&uacute;blica de estar limitada a recorrer ao fornecedor do seu software para fazer o suporte e a manuten&ccedil;&atilde;o.</p>
<p>Por outro lado, a Liberdade 2 garante a possibilidade de reutilizar o mesmo software independentemente do n&uacute;mero de computadores em que possa ser usados. Desta forma &eacute; poss&iacute;vel reduzir os custos n&atilde;o s&oacute; dentro de uma institui&ccedil;&atilde;o, como ao duplicar solu&ccedil;&otilde;es para outras institui&ccedil;&otilde;es dentro e fora da Administra&ccedil;&atilde;o P&uacute;blica.</p>
<p>A n&iacute;vel externo, o fomento da utiliza&ccedil;&atilde;o de Software Livre permitir&aacute; passar estas vantagens para o tecido empresarial e associativo portugu&ecirc;s, obtendo-se assim uma redu&ccedil;&atilde;o de custos que ajudar&aacute; as melhores empresas a evoluirem e o desenvolvimento de uma ind&uacute;stria nacional de Software Livre capaz de competir a n&iacute;vel mundial.</p>
<h2>
	Iniciativas Legislativas</h2>
<ul>
	<li>
		<span class="link-external"><a href="http://www.parlamento.pt/legis/inic_legis/20021004.09.1.0126.1.11" target="_self">Projecto de Lei apresentado pelo Bloco de Esquerda</a></span> sobre a utiliza&ccedil;&atilde;o de Software Livre na Administra&ccedil;&atilde;o P&uacute;blica (n&atilde;o aprovado)</li>
	<li>
		<span class="link-external"><a href="http://www.parlamento.pt/plc/docs/doc.pdf?path=6148523063446f764c3246795a5868774d53396b62324e77624331305a58683051584279623359764d6a41774e43395351564a664e6a5a664d6a41774e4335775a47593d&amp;fich=rar_66_2004.pdf" target="_self">Resolu&ccedil;&atilde;o da Assembleia da Rep&uacute;blica N&ordm; 66/2004</a></span> apresentada pelo PCP, recomenda ao Governo a tomada de medidas com vista ao desenvolvimento do Software Livre em Portugal</li>
</ul>
