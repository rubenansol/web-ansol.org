---
categories: []
metadata:
  event_location:
  - event_location_value: ULB campus Solbosh, Bruxelas, Bélgica
  event_site:
  - event_site_url: http://fosdem.org/
    event_site_title: FOSDEM
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-02-02 00:00:00.000000000 +00:00
    event_start_value2: 2013-02-03 00:00:00.000000000 +00:00
  node_id: 100
layout: evento
title: FOSDEM
created: 1351800256
date: 2012-11-01
---
<div id="slogan">
	A FOSDEM &eacute; um evento gratuito e sem necessidade de pr&eacute;-registo de participa&ccedil;&atilde;o que oferece &agrave;s comunidades de software livre um ponto de encontro para partilhar ideias e colaborar. &Eacute; famoso por ser muito orientado aos que colaboram no desenvolvimento de software livre e reune mais de 5 mil geeks do mundo inteiro.</div>
