---
categories:
- "#ubuntu-pt"
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 327
  node_id: 744
  event:
    location: https://meet.ubcasts.org/maio2020
    site:
      title: 
      url: 
    date:
      start: 2020-05-28 21:00:00.000000000 +01:00
      finish: 2020-05-28 23:45:00.000000000 +01:00
    map: {}
layout: evento
title: Encontro Ubuntu-pt @ Jitsi Meet
created: 1590401249
date: 2020-05-25
aliases:
- "/evento/744/"
- "/node/744/"
---
<p style="--original-color: #000000; --original-background-color: #ffffff;">Todos os meses, numa quinta-feira, a comunidade Ubuntu Portugal reúne-se no S̶a̶l̶o̶o̶n̶,̶ ̶e̶m̶ ̶S̶i̶n̶t̶r̶a̶.<br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);"><br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);">Atendendo à situação actual de saúde pública em que o nosso país ainda se encontra, este mês voltamos a encontrar-nos em linha e desta vez vamos aproveitar para apresentar a comunidade a quem nos conhece menos bem, as ferramentas para poderem contribuir para as tarefas da comunidade e a lista de tarefas que estão, nesta altura, a precisar de mais amor.<br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);"><br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);">Algumas das coisa que vamos (ajudar a) fazer:<br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);">Abrir 1 conta Ubuntu SSO;<br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);">Abrir conta Launchpad;<br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);">Assinar o código de conduta;<br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);">Conhecer os vários recursos utilizados pela Comunidade para contribuir para o Ubuntu e/ou tarefas relacionadas.<br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);"><br style="--original-color: #000000; --original-background-color: rgba(0, 0, 0, 0);">Vem, traz um amigo ou um familiar e vem conviver e partilhar experiências com o resto da comunidade portuguesa..</p><p style="--original-color: #000000; --original-background-color: #ffffff;">O ponto de encontro volta a ser o Jitsi (só precisam de um browser/navegador para web) no endereço:</p>
