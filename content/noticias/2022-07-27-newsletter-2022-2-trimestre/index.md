---
categories:
- newsletter
layout: article
title: Newsletter 2022 - 2º trimestre
date: 2022-07-27
---

No 2º trimestre de 2022 a ANSOL celebrou o Dia da Internet e colaborou com a
FSFE na tradução e divulgação de uma carta aberta sobre os direitos de software
em dispositivos conectados.

<!--more-->

## Direito Universal para instalar qualquer software em qualquer dispositivo

Assinámos e traduzimos para português a carta aberta sobre <strong>O Direito
Universal para instalar qualquer software em qualquer dispositivo</strong>:

> O desenho de software é crucial para o ecodesign e para a sustentabilidade de
> produtos e de hardware. Os sistemas e serviços de Software Livre permitem a
> reutilização, reaproveitamento e interoperabilidade de dispositivos. O
> direito universal de escolher livremente sistemas operativos, software e
> serviços é crucial para uma sociedade digital mais sustentável.

Podem ler a carta completa aqui: <https://fsfe.org/activities/upcyclingandroid/openletter.pt.html>


## Dia da Internet

Para celebrar o [Dia da Internet][ddi], várias comunidades ligadas à internet
ou em que esta é um ponto essencial da sua actividade juntaram-se para
conversar sobre o tema "Como tornar a Internet melhor".

O evento foi moderado por João Ribeiro, editor e jornalista no Shifter, e teve a presença de três convidados:

* Tiago Carrondo, pela ANSOL - [Associação Nacional para o Software Livre][ansol]
* Ricardo Lafuente, pela D3 - [Defesa dos Direitos Digitais][d3]
* André Barbosa, pela [Wikimedia Portugal][wmpt]

O vídeo do evento está disponível em <https://viste.pt/w/n1Y8f2mNuVtcNRLw8vn4Zp>


## Segue as actividades da ANSOL

Podem seguir as actividades através da nossa presença em várias redes:

- Segue-nos no Mastodon: <https://floss.social/@ansol>
- Segue-nos no twitter: <https://twitter.com/ANSOL>
- Junta-te à nossa sala via Matrix: <https://matrix.to/#/#geral:ansol.org>
- Vê os nossos repositórios de Software Livre: <https://git.ansol.org/ansol>

Se consideras importante o tema de Software Livre na sociedade, considera
[juntar-te à ANSOL](https://ansol.org/inscricao/). A ANSOL depende
exclusivamente da contribuição dos seus membros para suportar as suas
actividades, e gostaríamos de contar com a tua participação.



[ddi]: https://pt.wikimedia.org/wiki/Dia_da_Internet_2022
[d3]: https://direitosdigitais.pt
[wmpt]: https://pt.wikimedia.org/wiki/Wikimedia_Portugal
[ansol]: https://ansol.org
