---
categories: []
metadata:
  node_id: 73
layout: page
title: Apresentação à CECC - 2012/02/08
created: 1338329640
date: 2012-05-29
---
<p>Senhoras e senhores deputados. A ANSOL, Associa&ccedil;&atilde;o Nacional para o Software Livre, agradece esta oportunidade de expor a sua posi&ccedil;&atilde;o acerca do Projeto de Lei n&ordm; 118.</p>
<h3>
	IRRENUNCIABILIDADE E INALIENABILIDADE</h3>
<p>Um dos problemas deste Projeto de Lei &eacute; a irrenunciabilidade e inalienabilidade da compensa&ccedil;&atilde;o, estipulada no artigo quinto. Ainda que o Projeto de Lei exclua a sua aplicabilidade ao software em si, cobre toda a documenta&ccedil;&atilde;o t&eacute;cnica, imagens, sons e v&iacute;deos associados ao desenvolvimento do software, incluindo o Software Livre.</p>
<p>A impossibilidade legal de renunciar &agrave; compensa&ccedil;&atilde;o vai colocar obst&aacute;culos &agrave; participa&ccedil;&atilde;o em projetos importantes de Software Livre, como por exemplo aqueles geridos pela Free Software Foundation, por ser incompat&iacute;vel com as licen&ccedil;as desse software. &Eacute; de salientar que a ren&uacute;ncia aos direitos patrimoniais, neste caso, n&atilde;o acontece por press&atilde;o de alguma entidade que vise lucrar com o trabalho do autor, mas sim por clara op&ccedil;&atilde;o do autor.</p>
<p>Este artigo pode proteger os autores que vivem da venda de c&oacute;pias das suas obras mas retira a outros autores a possibilidade de dar um contributo volunt&aacute;rio e solid&aacute;rio para a inova&ccedil;&atilde;o na sociedade da informa&ccedil;&atilde;o.</p>
<p>Tal direito patrimonial deve continuar a poder ser renunciado, mas a manter-se o princ&iacute;pio, tem de existir uma exce&ccedil;&atilde;o para estes casos.</p>
<p>Poss&iacute;vel solu&ccedil;&atilde;o:</p>
<p><i>Artigo 5.&deg; - Inalienabilidade e irrenunciabilidade</i></p>
<p><i>Exceto quando a c&oacute;pia p&uacute;blica &eacute; autorizada diretamente pelos autores, artistas, int&eacute;rpretes ou executantes, a compensa&ccedil;&atilde;o equitativa &eacute; inalien&aacute;vel e irrenunci&aacute;vel, sendo nula qualquer cl&aacute;usula contratual em contr&aacute;rio.</i></p>
<h3>
	TAXAS</h3>
<p>A ANSOL op&otilde;e-se tamb&eacute;m &agrave; proposta de estender a taxa pela c&oacute;pia privada a equipamento inform&aacute;tico gen&eacute;rico, como discos r&iacute;gidos, cart&otilde;es de mem&oacute;ria e leitores digitais.</p>
<p>Curiosamente, este Projeto de Lei exclui explicitamente programas inform&aacute;ticos e bases de dados, mas ao mesmo tempo taxa o equipamento indispens&aacute;vel &agrave; implementa&ccedil;&atilde;o e uso de qualquer sistema inform&aacute;tico. Pior do que esta contradi&ccedil;&atilde;o, esta extens&atilde;o da taxa pela c&oacute;pia privada ir&aacute; prejudicar a grande maioria dos autores, maioria essa que est&aacute; fora do circuito de cobran&ccedil;a coletiva, por assentar numa premissa dogm&aacute;tica (sem se fundamentar em estudos econ&oacute;micos) de que a c&oacute;pia privada em equipamento inform&aacute;tico representa um preju&iacute;zo pelo qual alguns autores devem ser compensados.</p>
<p>A taxa pela c&oacute;pia privada aplicada a suportes como, por exemplo, CDs e cassetes virgens tem alguns defeitos, pois prejudica qualquer pessoa que queira usar esses suportes para divulgar as suas obras. Mas cumpre duas condi&ccedil;&otilde;es importantes que a podem justificar em alguns casos, tal como indic&aacute;mos em 2004, no processo de revis&atilde;o do C&oacute;digo do Direito de Autor e Direitos Conexos, do qual fizemos parte.</p>
<p>Primeiro, a compra de cassetes virgens servia atos legais como a c&oacute;pia privada ou outros que eram indiferentes para os autores que comercializassem as suas obras. Por exemplo, a c&oacute;pia de um disco comprado, o registo de obras pr&oacute;prias ou a c&oacute;pia de material em dom&iacute;nio p&uacute;blico. Mas nunca se provou que o com&eacute;rcio de cassetes virgens trouxesse, por si s&oacute;, algum benef&iacute;cio aos autores que vendessem as suas obras ao p&uacute;blico.</p>
<p>Em segundo lugar, sempre que algu&eacute;m comprava uma cassete virgem e a usava para copiar uma obra protegida, ao abrigo do direito &agrave; c&oacute;pia privada, era claro que o pre&ccedil;o que tinha pago pela cassete correspondia ao valor que atribu&iacute;a a essa c&oacute;pia. Cumpridas estas condi&ccedil;&otilde;es, pode parecer razo&aacute;vel defender que, em m&eacute;dia, parte do lucro da venda destes suportes fosse devido aos autores das obras copiadas, justificando-se uma taxa sobre estas vendas na condi&ccedil;&atilde;o de n&atilde;o prejudicar outros autores.</p>
<p>No armazenamento digital isto n&atilde;o acontece.</p>
<p>Em primeiro lugar, quando algu&eacute;m compra armazenamento digital e coloca l&aacute; o sistema operativo, software de processamento de texto, fotografias dos filhos, o email e o browser, etc., n&atilde;o &eacute; claro que, se eventualmente copiar para l&aacute; uma m&uacute;sica, o valor dessa c&oacute;pia seja proporcional &agrave; capacidade de armazenamento ou do seu valor como produto. Na verdade, dada a forma como acedemos, cada vez mais, ao conte&uacute;do online, o mais plaus&iacute;vel &eacute; que essa c&oacute;pia n&atilde;o tenha sequer valor monet&aacute;rio e que apenas l&aacute; esteja por ser gratuito, por ser trivial copiar um ficheiro.</p>
<p>Em segundo lugar, e ao contr&aacute;rio do que acontece com a cassete virgem, a c&oacute;pia privada n&atilde;o &eacute; uma motiva&ccedil;&atilde;o significativa para se comprar armazenamento digital; raramente algu&eacute;m vai comprar um armazenamento digital com o intuito de copiar os seus pr&oacute;prios CDs ou DVDs.</p>
<p>O armazenamento digital n&atilde;o serve apenas - nem maioritariamente - para o registo de obras sem prote&ccedil;&atilde;o ou para a c&oacute;pia privada. Serve para armazenar c&oacute;pias licenciadas e para aceder a todo o material promocional que, cada vez mais, os autores distribuem por via digital. O armazenamento digital &eacute; parte sine qua non da sociedade da informa&ccedil;&atilde;o, permitindo o com&eacute;rcio de software e das vendas online de m&uacute;sica, livros e v&iacute;deos, sendo necess&aacute;rio para visitar p&aacute;ginas dos artistas, alugar filmes e at&eacute; comprar bilhetes para concertos.</p>
<p>Portanto, contrabalan&ccedil;ando o alegado preju&iacute;zo da c&oacute;pia privada est&aacute; um benef&iacute;cio econ&oacute;mico que, segundo a nossa experi&ecirc;ncia, &eacute; cada vez mais claro e significativo para todos os autores e artistas, quer pela venda de obras em formato digital, quer na auto-promo&ccedil;&atilde;o dos artistas.</p>
<p>Quanto mais equipamento inform&aacute;tico o p&uacute;blico tiver, maior &eacute; o mercado acess&iacute;vel aos autores, e maior o benef&iacute;cio econ&oacute;mico para estes.</p>
<p>A c&oacute;pia privada legal, que &eacute; aquilo que esta lei contempla, est&aacute; pensada para o tempo em que algu&eacute;m comprava um disco e o copiava para uma cassete para ouvir no walkman. Hoje em dia isso pouco acontece, e acontecer&aacute; cada vez menos, explicando em grande parte a queda de vendas de &aacute;lbuns musicais e filmes: hoje em dia &eacute; poss&iacute;vel comprar e alugar ficheiros, por exemplo online ou na televis&atilde;o, j&aacute; pagando a licen&ccedil;a para usar esse conte&uacute;do em um ou v&aacute;rios aparelhos.</p>
<p>Cada vez mais a c&oacute;pia legal &eacute; uma c&oacute;pia autorizada, paga e licenciada, que, n&atilde;o cabendo no &acirc;mbito da c&oacute;pia privada, n&atilde;o pode representar preju&iacute;zo nem carece de compensa&ccedil;&atilde;o adicional.</p>
<p>Al&eacute;m disso, a tecnologia digital expandiu imenso o n&uacute;mero de autores que fixam e publicam as suas obras.</p>
<p>Nas redes sociais, nos blogs, nos sites de partilha de fotografias e de v&iacute;deos vemos um volume enorme de obras originais, publicadas, protegidas por direitos de autor mas cujos autores, em n&uacute;mero muito superior ao dos benefici&aacute;rios desta taxa, ir&atilde;o apenas pagar pela sua criatividade sem receber nada em troca, quer seja porque criam pelo gosto de criar ou porque fazem parte do grupo crescente de profissionais que autorizam a c&oacute;pia, recorrendo a esquemas alternativos de licenciamento.</p>
<p>Por isso, a grande maioria dos autores ter&aacute; nesta taxa apenas preju&iacute;zo em vez de compensa&ccedil;&atilde;o.</p>
<h3>
	DRM</h3>
<p>Finalmente, este Projeto de Lei considera o suporte digital apenas para o taxar, perdendo a oportunidade de assegurar devidamente o Art. 6&deg; ponto 4 da diretiva 2001/29/CE, corrigindo um erro grave na legisla&ccedil;&atilde;o vigente.</p>
<p>Se em teoria todos temos o direito a aceder aos conte&uacute;dos que compramos e &agrave; sua c&oacute;pia privada, na pr&aacute;tica os cidad&atilde;os s&oacute; podem exercer esses direitos se a editora n&atilde;o os limitar com medidas de car&aacute;cter tecnol&oacute;gico ou medidas tecnol&oacute;gicas eficazes, vulgo DRM, que n&atilde;o s&oacute; impedem a c&oacute;pia mas tamb&eacute;m o pr&oacute;prio acesso aos conte&uacute;dos em plataformas alternativas.</p>
<p>Por exemplo, n&atilde;o &eacute; poss&iacute;vel usufruir legalmente de muitos conte&uacute;dos comprados em suportes originais recorrendo a Software Livre por terem DRM.</p>
<p>Se bem que, tecnologicamente, seja uma tarefa trivial, legalmente &eacute; punido com at&eacute; um ano de pris&atilde;o, porque exige que se contorne as medidas de restri&ccedil;&atilde;o digital que as editoras implementam nesses suportes.</p>
<p>Adicionalmente, n&atilde;o &eacute; pragm&aacute;tico acreditar que a IGAC tenha os meios para obrigar que todas as obras que recebe em dep&oacute;sito se encontrem livres de DRM, a &uacute;nica forma de cumprir o papel de controlo de abusos da lei previsto no ponto 3 do artigo 221&deg; do CDADC.</p>
<p>Por isso, a ANSOL prop&otilde;e tamb&eacute;m que este Projeto de Lei, que visa atualizar a legisla&ccedil;&atilde;o da c&oacute;pia privada ao dom&iacute;nio digital, permita explicitamente que o DRM possa ser contornado para fins legais, como a c&oacute;pia privada e o natural acesso &agrave; obra legitimamente adquirida, usando o equipamento e software que o comprador entender.</p>
<p>Poss&iacute;vel solu&ccedil;&atilde;o:</p>
<p><i>Altera&ccedil;&atilde;o do Artigo 221&deg; ponto 3 do CDADC para:</i></p>
<p><i>3 - Sempre que se verifique em raz&atilde;o de omiss&atilde;o de conduta, que uma medida eficaz de car&aacute;cter tecnol&oacute;gico impede ou restringe o uso ou a frui&ccedil;&atilde;o de uma utiliza&ccedil;&atilde;o livre por parte de um benefici&aacute;rio que tenha legalmente acesso ao bem protegido, pode o lesado neutralizar a medida eficaz de car&aacute;cter tecnol&oacute;gico.</i></p>
<h3>
	CONCLUS&Atilde;O</h3>
<p>Por estes motivos, propomos:</p>
<ul>
	<li>
		que o artigo quinto permita as exce&ccedil;&otilde;es necess&aacute;rias para que qualquer autor possa prescindir dos seus direitos patrimoniais;</li>
	<li>
		que a taxa sobre equipamento inform&aacute;tico seja removida do Projeto de Lei n&ordm;118;</li>
	<li>
		que se garanta o cumprimento da Diretiva Europeia, permitindo a neutraliza&ccedil;&atilde;o de medidas tecnol&oacute;gicas que impe&ccedil;am o cidad&atilde;o de usufruir legalmente da obra;</li>
	<li>
		Que tenham em conta os mais de sete mil cidad&atilde;os subscritores da peti&ccedil;&atilde;o por n&oacute;s lan&ccedil;ada, em t&atilde;o curto espa&ccedil;o de tempo.</li>
</ul>
<p>Obrigado a todos pela vossa aten&ccedil;&atilde;o, esperamos ter sido claros, mas estamos dispon&iacute;veis aqui, bem como em mais reuni&otilde;es, ou at&eacute; contactos por email, que julguem necess&aacute;rios para vos ajudar a esclarecer este e outros assuntos.</p>
