---
categories:
- evento
- volt
- livre
- ansol
- nomadismo digital
- noticia
metadata:
  tags:
  - tags_tid: 84
  - tags_tid: 337
  - tags_tid: 338
  - tags_tid: 33
  - tags_tid: 339
  node_id: 805
layout: article
title: ANSOL participa em evento sobre Literacia Digital
created: 1624485332
date: 2021-06-23
aliases:
- "/article/805/"
- "/node/805/"
---
Num convite feito pelo VOLT Portugal, a ANSOL esteve presente num evento sobre
Literacia Digital, onde Tiago Carrondo, presidente da ANSOL, falou sobre a
importância do Software Livre.

<!--more-->

O evento, co-organizado com o Livre, teve também a particição de Krystel Leal,
freelancer em Silicon Valley na área de desenvolvimento web e fundadora do
projecto Nomadismo Digital Portugal, e Paulo Maia, Mestre em Bioengenharia a
exercer agora como data scientist.

A gravação do evento pode ser vista aqui:

<iframe src="https://archive.org/embed/hoje-vamos-falar-de-literacia-digital-explicando-a-importancia-de-softwares-...-322351522628976"
        width="640" height="480"
        frameborder="0"></iframe>
