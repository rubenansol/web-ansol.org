---
categories: []
metadata:
  event_site:
  - event_site_url: http://datewithdata.pt/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-06-10 23:00:00.000000000 +01:00
    event_start_value2: 2016-06-10 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 423
layout: evento
title: 'Date With Data #9'
created: 1463154308
date: 2016-05-13
---
<div><span>Este mês o Date With Data volta a arregaçar as mangas e meter as mãos na <a href="http://transparenciahackday.us6.list-manage.com/track/click?u=7c39a9eb2d8160fd5975e6fec&amp;id=38a94d5214&amp;e=b684bb14e9" target="_blank" style="color: #2baadf; font-weight: normal; text-decoration: underline;" data-saferedirecturl="https://www.google.com/url?q=http://transparenciahackday.us6.list-manage.com/track/click?u%3D7c39a9eb2d8160fd5975e6fec%26id%3D38a94d5214%26e%3Db684bb14e9&amp;source=gmail&amp;ust=1465488596215000&amp;usg=AFQjCNEWLkrLdAmsfG53VnWWAFkomc8IXg">Central de Dados</a>. B</span><span>em perto</span><span> de chegar a uma versão 1.0 deste portal para publicar conjuntos de dados aberto</span><span>s</span><span>, ainda faltam fechar umas quantas entradas da lista de tarefas.</span></div><div>&nbsp;</div><div><div><span>A Central de Dados é um repositório público são partilhados dados abertos que foram sendo recolhidos e tratados , ao longo dos anos. O objectivo é </span><span>criar uma plataforma leve, simples de instalar, simples de usar, seguindo as normas propostas pela <a href="http://transparenciahackday.us6.list-manage.com/track/click?u=7c39a9eb2d8160fd5975e6fec&amp;id=3a45cb1571&amp;e=b684bb14e9" target="_blank" style="color: #2baadf; font-weight: normal; text-decoration: underline;" data-saferedirecturl="https://www.google.com/url?q=http://transparenciahackday.us6.list-manage.com/track/click?u%3D7c39a9eb2d8160fd5975e6fec%26id%3D3a45cb1571%26e%3Db684bb14e9&amp;source=gmail&amp;ust=1465488596215000&amp;usg=AFQjCNF5MgTX06d63Hz2kw0_Yi7nk4RmLw">Open Knowledge International</a> para a distribuição de dados com licenças livres.&nbsp;</span></div></div>
