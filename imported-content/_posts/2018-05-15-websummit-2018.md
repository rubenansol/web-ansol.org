---
categories: []
metadata:
  event_location:
  - event_location_value: Altice Arena, Lisboa
  event_site:
  - event_site_url: https://websummit.com/
    event_site_title: WebSummit 2018
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-11-05 00:00:00.000000000 +00:00
    event_start_value2: 2018-11-08 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 622
layout: evento
title: WebSummit 2018
created: 1526419912
date: 2018-05-15
---
<p class="lead">Web Summit started as a simple idea in 2010: Let’s connect the technology community with all industries, both old and new. It seemed to resonate. Web Summit has grown to become the “largest technology conference in the world”.</p><p class="lead">No conference has ever grown so large so fast. But we also pride ourselves in organising the “best technology conference on the planet”.</p>
