---
categories: []
metadata:
  event_site:
  - event_site_url: https://www.nao-ao-ttip.pt/jantar-debate-dia-21-de-fevereiro-fabrica-de-alternativas/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-02-21 00:00:00.000000000 +00:00
    event_start_value2: 2015-02-21 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 283
layout: evento
title: O que é o TTIP?
created: 1423827405
date: 2015-02-13
---
<p><strong><span style="font-size: large;"><strong>21 de Fevereiro, </strong></span><strong><span style="font-size: large;">no espaço da </span></strong><strong>Fábrica de Alternativas</strong><br> </strong><a href="https://www.google.pt/maps/place/Largo+Madalena,+1495+Alg%C3%A9s/@38.7040318,-9.2331909,16z/data=%214m20%211m17%214m16%211m6%211m2%211s0xd1ecb7c098ab3e5:0x25dfa90ed5608f6f%212sAlg%C3%A9s+-+Mercado%212m2%211d-9.22801%212d38.701126%211m6%211m2%211s0xd1ecc81e5e92b09:0xdd67664648ac220%212sLargo+Madalena%212m2%211d-9.2297984%212d38.7068614%213e2%215i1%213m1%211s0xd1ecc81e5e92b09:0xdd67664648ac220" title="Fábrica de Alternativas">Largo Madalena- Algés</a></p><p><strong><span style="font-size: large;">20h00 &gt;</span> &nbsp;jantar vegetariano</strong><strong><br> </strong><strong>21h30<span style="font-size: large;"> &gt;</span> Debate</strong></p><p>&nbsp;</p>
