---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: https://websummit.net/fullstk
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-11-07 00:00:00.000000000 +00:00
    event_start_value2: 2016-11-10 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 470
layout: evento
title: Web Summit - FullSTK
created: 1476895849
date: 2016-10-19
---
<p>A FullSTK é uma das dezenas de conferências a decorrer este ano no Web Summit, em Lisboa. Uma das suas tracks é sobre <strong>Open Source</strong>, e nela irá falar, entre outros, Richard Stallman, o criador do movimento do Software Livre.</p><p>A apresentação de Richard Stallman será dia 10 às 16:10. Mais informação <a href="https://www.fsf.org/events/rms-20161110-lisbon">aqui</a>.</p>
